/*
 * @Author: 陈铭
 * @Date: 2020-06-08 09:35:59
 * @Last Modified by: 陈铭
 * @Last Modified time: 2020-06-08 15:05:28
 */
export default [
  {
    url: '/getHello',
    type: 'post',
    response: (req) => {
      const msg = req.body

      let data = `wrong word: ${msg}`

      if (msg.toLocaleLowerCase() === 'world') {
        data = 'hello'
      }

      return {
        data: data
      }
    }
  }
]
