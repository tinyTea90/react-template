/*
 * @Author: 陈铭
 * @Date: 2020-06-08 09:18:02
 * @Last Modified by: 陈铭
 * @Last Modified time: 2020-12-22 14:04:38
 */
import Mock from 'mockjs'
import mocks from './mock.js'

Mock.setup({
  timeout: '200-600'
})

mocks.map((mock) => {
  // url路径字符串与请求路径进行匹配，路径和请求路径的末尾相同;如果后面有拼接字符串，则判断问号之前的路径
  const reMsg = mock.url.replace(/\//g, '\\/')
  const re = RegExp(`(${reMsg}\\?)|(${reMsg})$`)
  Mock.mock(new RegExp(re), mock.type || 'get', mock.response)
})
