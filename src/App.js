/*
 * @Author: 陈铭
 * @Date: 2021-06-25 16:52:19
 * @LastEditors: 陈铭
 * @LastEditTime: 2021-06-28 10:22:23
 * @Description: file content
 * @FilePath: /React/my-react/src/App.js
 */
import { Component } from 'react'
import './App.css'
import { renderRoutes } from 'react-router-config'
import { Link } from 'react-router-dom'
import { object } from 'prop-types'

export default class App extends Component {
  static propTypes = {
    route: object
  }

  render() {
    return (
      <div className="App">
        <h1>this is a title</h1>
        <Link to="/helloworld">hello world</Link>&nbsp;
        <Link to="/about">about</Link>
        <div>{renderRoutes(this.props.route.routes)}</div>
      </div>
    )
  }
}
