/**
 * @name: index.js
 * @author: 陈铭
 * @date: 2021-08-30 11-02
 * @description：路由
 */
import { Redirect } from 'react-router-dom'

import HelloWorld from '@/views/HelloWorld/HelloWorld'
import App from '@/App'
import About from '@/views/About/About'

const routes = [
  {
    path: '/',
    component: App,
    routes: [
      {
        path: '/',
        exact: true,
        render: () => <Redirect to="/helloworld"></Redirect>
      },
      {
        path: '/helloworld',
        component: HelloWorld
      },
      {
        path: '/about',
        component: About
      }
    ]
  }
]

export default routes
