/**
 * @name: ip.js
 * @author: 陈铭
 * @date: 2021-08-30 14-25
 * @description：获取ip
 */
const os = require('os')

exports.ip = (function () {
  let needHost = ''
  try {
    let network = os.networkInterfaces()
    for (const dev in network) {
      let iface = network[dev]
      for (let i = 0; i < iface.length; i++) {
        let alias = iface[i]
        if (
          alias.family === 'IPv4' &&
          alias.address !== '127.0.0.1' &&
          !alias.internal
        ) {
          needHost = alias.address
        }
      }
    }
  } catch (e) {
    needHost = 'localhost'
  }
  return needHost
})()
