import Axios from 'axios'

/*
 * @Author: 陈铭
 * @Date: 2020-06-08 10:48:25
 * @Last Modified by: 陈铭
 * @Last Modified time: 2020-06-08 15:06:37
 */
const service = Axios.create({
  baseURL: process.env.REACT_APP_MAIN_PATH,
  withCredentials: true,
  timeout: 20000
})

// 预处理请求返回
service.interceptors.response.use(
  (response) => {
    const { status, statusText, data } = response
    if (status === 200) {
      return data
    } else {
      return Promise.reject(new Error(`error status: ${statusText}`))
    }
  },
  (error) => {
    return Promise.reject(new Error(`request error: ${error}`))
  }
)

export default service
