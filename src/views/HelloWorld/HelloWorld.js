/*
 * @Author: 陈铭
 * @Date: 2021-06-25 21:41:15
 * @LastEditors: 陈铭
 * @LastEditTime: 2021-06-28 11:15:04
 * @Description: file content
 * @FilePath: /React/my-react/src/views/HelloWorld/HelloWorld.js
 */
import { Component } from 'react'
import { Link } from 'react-router-dom'
import { string } from 'prop-types'

export class HelloWorld extends Component {
  state = {
    text: 'hello world'
  }

  static defaultProps = { sex: 'male' }
  static propTypes = {
    sex: string
  }

  componentDidMount() {
    this.setState({ text: 'hello new' })
    console.log(this.state.text)

    const domBtn = document.getElementById('changeTextBtn')
    domBtn.onclick = () => {
      this.setState({ text: 'change again' })
      console.log(this.state.text)
    }
  }

  handleClick = async () => {
    await this.setState({ text: 'hello old' })
    console.log(this.state.text)
    setTimeout(() => {
      this.setState({ text: 'hello old 2' })
      console.log(this.state.text)
    }, 2000)
  }

  render() {
    return (
      <div>
        <h1 onClick={this.handleClick}>{this.state.text}</h1>
        <p>{this.props.sex}</p>
        <div>
          <button id="changeTextBtn">change text</button>
        </div>
        <Link to="/about">about</Link>
      </div>
    )
  }
}

export default HelloWorld
