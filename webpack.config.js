/**
 * @name: webpack.config.js
 * @author: 陈铭
 * @date: 2021-08-29 17-57
 * @description：webpack配置
 */
const path = require('path')
const { ip } = require('./src/utils/ip')
const webpack = require('webpack')
const HtmlPlugin = require('html-webpack-plugin')
const InterpolateHtmlPlugin = require('interpolate-html-plugin')
const Dotenv = require('dotenv-webpack')
const { CleanWebpackPlugin } = require('clean-webpack-plugin')
const UglifyJsPlugin = require('uglifyjs-webpack-plugin')
const FriendlyErrorsWebpackPlugin = require('friendly-errors-webpack-plugin')
const CopyWebpackPlugin = require('copy-webpack-plugin')
const FileManagerPlugin = require('filemanager-webpack-plugin')
const packageConfig = require('./package.json')
const nowDate = new Date()
const { env } = process

const config = {
  mode: env.REACT_APP_ENV,
  devtool: 'inline-source-map',
  entry: {
    index: './src/index.js'
  },
  output: {
    filename: 'bundle[chunkhash:8].js',
    path: path.resolve(__dirname, 'build')
  },
  resolve: {
    alias: {
      '@': path.resolve(__dirname, 'src')
    }
  },
  module: {
    rules: [
      {
        test: /\.css$/,
        use: [{ loader: 'style-loader' }, { loader: 'css-loader' }]
      },
      {
        test: /\.scss$/,
        use: [
          { loader: 'style-loader' },
          {
            loader: 'css-loader',
            options: { modules: true, importLoaders: 1 }
          },
          { loader: 'sass-loader' }
        ]
      },
      {
        test: /\.(png|svg|jpg|gif)$/,
        loader: 'url-loader',
        options: {
          limit: 10000,
          name: 'img/[name].[hash:7].[ext]'
        }
      },
      {
        test: /\.(js|jsx)$/,
        use: [
          {
            loader: 'babel-loader',
            options: { plugins: ['@babel/plugin-transform-runtime'] }
          }
        ],
        exclude: /node_modules/
      }
    ]
  },
  devServer: {
    port: env.REACT_APP_PORT
  },
  plugins: [
    new webpack.HotModuleReplacementPlugin(),
    new HtmlPlugin({
      template: 'public/index.html',
      filename: './index.html'
    }),
    new InterpolateHtmlPlugin({
      PUBLIC_URL: 'static',
      PROJECT_TITLE: packageConfig.title
    }),
    new Dotenv({
      path: path.resolve(__dirname, `.env.${env.REACT_APP_ENV}`)
    }),
    new webpack.ProvidePlugin({
      React: 'react',
      process: 'process/browser'
    }),
    new CopyWebpackPlugin({
      patterns: [
        {
          from: path.resolve(__dirname, 'public'),
          to: 'static'
        }
      ]
    }),
    new CleanWebpackPlugin()
  ],
  stats: 'minimal'
}

if (env.NODE_ENV === 'backserver') {
  config['devServer'] = {
    ...config.devServer,
    overlay: {
      warnings: false,
      errors: true
    },
    proxy: {
      [env.REACT_APP_MAIN_PATH]: {
        target: `http://${env.REACT_APP_IP}:${env.REACT_APP_PORT}${env.REACT_APP_MAIN_PATH}`,
        changeOrigin: true,
        pathRewrite: {
          [`^${env.REACT_APP_MAIN_PATH}`]: ''
        }
      }
    }
  }
}

if (env.REACT_APP_ENV === 'production') {
  config['performance'] = {
    //打包文件大小配置
    maxEntrypointSize: 10000000,
    maxAssetSize: 30000000
  }
  config.plugins.push(
    new UglifyJsPlugin({
      uglifyOptions: {
        compress: {
          drop_console: true
        }
      },
      sourceMap: false,
      parallel: true
    }),
    new FileManagerPlugin({
      events: {
        onEnd: {
          delete: ['./zip'],
          archive: [
            {
              source: './dist',
              destination: `./zip/${
                packageConfig.name
              }_${nowDate.getFullYear()}${
                nowDate.getMonth() + 1
              }${nowDate.getDate()}_v.${packageConfig.version}.zip`
            }
          ]
        }
      }
    })
  )
} else {
  config.plugins.push(
    new FriendlyErrorsWebpackPlugin({
      compilationSuccessInfo: {
        notes: [`IP: http://${ip}:${env.REACT_APP_PORT}`]
      }
    })
  )
}

module.exports = config
